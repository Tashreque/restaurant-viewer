import UIKit
import CoreLocation

class RestaurantListController: UIViewController {
    
    // Location services manager.
    let locationManager = CLLocationManager()
    var latitude: Double?
    var longitude: Double?
    
    // The main table view which lists all restaurants.
    @IBOutlet weak var restaurantTableView: UITableView!
    @IBOutlet weak var sideMenuView: UIView!
    
    // Constraints defined for animations.
    @IBOutlet weak var restaurantListLeadingSpace: NSLayoutConstraint!
    @IBOutlet weak var restaurantListTrailingSpace: NSLayoutConstraint!
    
    
    // Array of restaurant contents (Computed property).
    var restaurantList: [CellContent] = [] {
        didSet {
            DispatchQueue.main.async {
                self.restaurantTableView.reloadData()
            }
        }
    }
    
    // Side menu displayed?
    var sideMenuDisplayed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check if location services is enabled, if not,
        // enable necessary features.
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
    }
    
    // Side menu.
    @IBAction func sideMenuButtonPressed(_ sender: UIBarButtonItem) {
        if sideMenuDisplayed {
            restaurantListLeadingSpace.constant = 0
            restaurantListTrailingSpace.constant = 0
        } else {
            restaurantListLeadingSpace.constant = 250
            restaurantListTrailingSpace.constant = -250
        }
        
        // Do slide in animation.
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        
        // Change side menu display state.
        sideMenuDisplayed = !sideMenuDisplayed
    }
    
    // Fetch restaurant information.
    func fetchRestaurantInformation() {
        if let lat = self.latitude, let long = self.longitude {
            let restaurantInfoObject = RestaurantInfo()
            let _ = restaurantInfoObject.getRestaurantInfo(latitude: lat, longitude: long) { (placeInfo) in
                self.fillRestaurantList(names: placeInfo.0, statuses: placeInfo.1, distances: placeInfo.2, ratings: placeInfo.3, photoURLs: placeInfo.4)
                
            }
            print("Info fetched!")
        } else {
            print("Did not get location coordinates yet!")
        }
    }
    
    // Fill restaurantList array with restaurants.
    func fillRestaurantList(names: [String], statuses: [String], distances: [String], ratings: [String], photoURLs: [String]) {
        
        // Fill table cells, but with all images set to nil.
        let total = names.count
        for i in 0..<total {
            let content = CellContent(image: UIImage(named: "tableCellPlaceHolder")!, name: names[i], status: statuses[i], distance: distances[i], rating: ratings[i], imageURL: photoURLs[i])
            restaurantList.append(content)
        }
    }
}

// Extension that contains all handler functions.
extension RestaurantListController: UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
    
    // Get number of tableview rows.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurantList.count
    }
    
    // Load tableview rows.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let content = restaurantList[indexPath.row]
        let cell = restaurantTableView.dequeueReusableCell(withIdentifier: "CellController") as! CellController
        cell.setContent(content: content)
        cell.restaurantImageView.downloadImage(from: content.imageURL)
        
        return cell
    }
    
    // Deselect row after being tapped.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // Get current location coordinates.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let locationValue: CLLocationCoordinate2D = manager.location?.coordinate {
            let lat = locationValue.latitude
            let long = locationValue.longitude
            print("\(locationValue.latitude), \(locationValue.longitude)")
            
            latitude = lat
            longitude = long
            
            // Get latitude, longitude and obtain weather info.
            fetchRestaurantInformation()
            
        }
    }
}
