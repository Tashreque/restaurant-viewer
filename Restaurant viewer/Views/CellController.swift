import UIKit

class CellController: UITableViewCell {
    
    // Cell outlet variables
    @IBOutlet weak var restaurantImageView: ImageViewWithAsyncDownload!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var restaurantStatuslabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    // Content setter for the cell
    func setContent(content: CellContent) {
        self.restaurantImageView.image = content.restaurantImage
        
        self.restaurantNameLabel.text = content.restaurantName
        
        self.restaurantStatuslabel.text = content.restaurantStatus
        //Check if open or closed.
        if content.restaurantStatus == "Closed" {
            self.restaurantStatuslabel.textColor = .red
        } else {
            self.restaurantStatuslabel.textColor = .green
        }
        
        self.distanceLabel.text = content.distance
        self.ratingLabel.text = content.rating
    }

}
