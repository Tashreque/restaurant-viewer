import Foundation
import UIKit

class RestaurantInfo {
    
    var searchRadius = "1500"
    let apiKey = "AIzaSyAveGBh5kJBOsNN_HE4VnpohU83x7OoLkc"
    
    // Initializer.
    init() {
        print("API fetching called.")
    }
    
    // Get general restaurant information.
    func getRestaurantInfo(latitude: Double, longitude: Double, completion: @escaping (([String], [String], [String], [String], [String])) -> ()) {
        let restaurantRequest = URL(string: "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(latitude),\(longitude)&radius=\(searchRadius)&type=restaurant&key=\(apiKey)")!
        
        // Start api decoding.
        let session = URLSession.shared
        let task = session.dataTask(with: restaurantRequest) { (data, response, error) in
            guard let parsedData = data else {
                print("Error!")
                return
            }
            
            guard error == nil else {
                print("Error!")
                return
            }
            do {
                let decodedData = try JSONDecoder().decode(PlaceResponse.self, from: parsedData)
                
                // Restaurant information containers.
                var names: [String] = []
                var statuses: [String] = []
                var distances: [String] = []
                var ratings: [String] = []
                var photoURLs: [String] = []
                
                for place in decodedData.results {
                    
                    // Get name.
                    let name = place.name
                    names.append(name)
                    
                    // Get open/closed status.
                    var openOrNot = ""
                    if let status = place.openingHours?.isOpen {
                        if status == true {
                            openOrNot = "Open"
                        } else {
                            openOrNot = "Closed"
                        }
                    }
                    statuses.append(openOrNot)
                    
                    // Get coordinates and calculate distance.
                    let distance = "0.1 mi"
                    distances.append(distance)
                    
                    // Get rating.
                    var rating = 0.0
                    if let r = place.rating {
                        rating = r
                    }
                    ratings.append(String(rating))
                    
                    // Temporary photo information variables.
                    let photoReference = place.photos[0].photoReference
                    //let height = place.photos[0].height
                    let width = place.photos[0].width
                    
                    // Make photo URL, append to URL list.
                    let photoURLString = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=\(width)&photoreference=\(photoReference)&key=\(self.apiKey)"
                    photoURLs.append(photoURLString)
                    
                }
                
                // Pass contents to restaurant list controller.
                completion((names, statuses, distances, ratings, photoURLs))
                
            } catch {
                print("Parsing error!")
            }
        }
        task.resume()
    }
}
