import Foundation
import UIKit

class CellContent {
    
    // Class attributes
    var restaurantImage: UIImage
    var restaurantName: String
    var restaurantStatus: String
    var distance: String
    var rating: String
    var imageURL: String
    
    // Constructor/Initializer
    init(image: UIImage, name: String, status: String, distance: String, rating: String, imageURL: String) {
        
        self.restaurantImage = image
        self.restaurantName = name
        self.restaurantStatus = status
        self.distance = distance
        self.rating = rating
        self.imageURL = imageURL
    }
    
}

