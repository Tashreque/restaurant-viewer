//
//  ImageViewWithAsyncDownload.swift
//  Restaurant viewer
//
//  Created by Apple on 9/12/19.
//  Copyright © 2019 Tashreque Haq. All rights reserved.
//

import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

class ImageViewWithAsyncDownload: UIImageView {
    
    var task: URLSessionDataTask!
    
    func downloadImage(from url: String) {
        
        // Stop current task.
        if let task = task {
            task.cancel()
        }
        
        // Check if image already in cache.
        if let imageFromCache = imageCache.object(forKey: url as AnyObject) as? UIImage {
            self.image = imageFromCache
            return
        }
        
        let downloadURL = URL(string: url)!
        
        let session = URLSession.shared
        task = session.dataTask(with: downloadURL) { (data, response, error) in
            guard error == nil else {
                print("Image fetch error!")
                return
            }
            
            guard let imageData = data else {
                print("Image parsing error!")
                return
            }
            
            // Retrieve image from image data.
            let restaurantImage = UIImage(data: imageData)
            if let image = restaurantImage {
                // Update image view with the retrieved image.
                imageCache.setObject(image, forKey: url as AnyObject)
                DispatchQueue.main.async {
                    self.image = image
                }
            } else {
                print("Could not fetch image!")
            }
        }
        task.resume()
    }
}
